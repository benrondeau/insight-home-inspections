<!DOCTYPE html>
<html>
<head>
 
    <title>About Us | Insight Inspections - Austin, TX</title>

    <!-- Import Header Links -->
    <?php include 'header.php'; ?>
    
</head>
<body>

    <!-- Header Menu -->
    <?php include 'header_nav.php'; ?> 

<div class="content-area"> 
    <div class="row service-statement aboutbg">
        <div class="pattern2"></div>
        <h1>Meet The Team</h1>
    </div>
    <div class="row team lightgradbg">
        <div class="content">
            <div class="row team-sub">
                <div class="c-12">
                    <h2>Why Us?</h2>
                    <p>A home inspection is only as good as the inspector. That’s why Insight Inspections has a team of the most experienced and trained home inspectors in Texas. Whether you’re a homebuyer, seller, or homeowner, we’ll provide you with a detailed description of a property’s condition so you can move forward confidently and with peace of mind.</p>
                </div>
            </div>
            <div class="row team-sub">
                <div class="c-4">
                    <img src="images/scott.jpg">
                </div>
                <div class="c-8">
                    <h2>Scott Swayze</h2>
                    <p>Welcome, and thank you for taking a moment and visiting our website. My name is Scott Swayze and I am a licensed professional inspector in the state of Texas (#21271). I started Insight Inspections with the goal of educating home buyers about the current condition of their upcoming real estate investment. Having spent several years as a general contractor/ project manager, my experience and education has given me the ability to properly evaluate your biggest investment. Through detailed assessment, clear communication and state of the art technology I pride myself on the ability to provide my clients with all the information needed to make an informed real estate decision. Have a question? I would love to hear from you. I can be reached anytime at scott@austinhomeinsight.com or 512-638-7023</p>
                </div>
            </div>
            <div class="row team-sub">
                <div class="c-4">
                    <img src="images/nathan.jpg">
                </div>
                <div class="c-8">
                    <h2>Nathan Rowell</h2>
                    <p>Hey there! My name is Nathan Rowell and I am a licensed professional inspector here in the state of Texas (TREC # 21456). I have a strong background in storm restoration, general real estate repair and work experience in exterior home improvements. I am very knowledgeable of home systems and inspection practices, and most people notice my charisma/friendliness with all of my clients. I'm flexible so I would love to answer questions and educate home buyers about the condition of their upcoming investment. Additionally, I'm always available, so if you have a question I would love to hear from you! You can email me anytime at nathan@austinhomeinsight.com or feel free to call me directly at 512-680-7268</p>
                </div>
            </div> 
            <div class="row team-sub">
                <div class="c-12">
                    <h2>The Insight Advantage</h2>
                    <p>The decision to purchase a home is one of the largest and most exciting financial decisions of your life. To make the most informed decision possible it's important to have an accurate, complete picture of the home you are interested in purchasing. Finding a home inspector that you trust is an essential part of that process. We inspect every home with the utmost care, and take time to explain our findings clearly while answering all of your questions and concerns. Your home inspector will provide you with a comprehensive report, complete with high-quality photos as well as detailed descriptions for every major component of your home or business. A state of the art thermal imaging scan is included on EVERY Insight inspection and with our 100% satisfaction guarantee, you can be sure an Insight inspection will give you all the information needed to make an educated real estate decision.</p>
                </div>
            </div>
            <div class="row team-sub">
                <div class="c-12">
                    <h2>100% SATISFACTION GUARANTEE</h2>
                    <p>If for any reason you are not 100% satisfied with your home inspection, we’ll either make it right, or you don’t pay for the inspection! We are so confident you will value our inspections that your satisfaction is guaranteed for sixty (60) days from date of inspection.</p>
                </div>
            </div>            
        </div>
    </div>
    <div class="row badges">
        <div class="content">
            <div class="c-20 frame1">
                <a href="http://www.nachi.org/verify.php?nachiid=NACHI14103014">
                    <img src="http://www.nachi.org/images/logos-banners/seal-t.gif?nachiid=NACHI14103014" width="98" height="102" title="InterNachi Certified" alt="Certified by the International Association of Certified Home Inspectors" border="0"></a>
            </div>
            <div class="c-20 frame2">
                <img src="images/InfraredLogo.png" title="Infrared Certified" alt="Infrared Certified logo">
            </div>
            <div class="c-20 frame3">
                <a href="http://www.angieslist.com/AngiesList/Review/8455220" title="Angies List - Insight Inspections"><img src="http://www.angieslist.com/webbadge/PurlImage.ashx?bid=1016639e074aad9f97e4198eb3df7e8e" alt="Read Unbiased Consumer Reviews Online at AngiesList.com" width="200"></a>
            </div>
            <div class="c-20 frame4">
                <a href="http://www.yelp.com/biz/insight-inspections-austin" title="Yelp - Insight Inspections"><img alt="Insight Inspections" src="http://dyn.yelpcdn.com/extimg/en_US/rrc/kQGJof8Sj5DVWk9ZMzj5Mg.png" height="55" width="125"></a>
            </div>
            <div class="c-20 frame5">
                <a href="" title="BBB - Insight Inspections"><img alt="Insight Inspections" src="images/bbb.jpg" width="150"></a>
            </div>            
        </div>
    </div>  
</div><!--End Content-Area-->  
        
   <!-- Import Footer Links -->
    <?php include 'footer_menu.php'; ?>  

    <!-- Import Footer Links -->
    <?php include 'footer.php'; ?> 
         
</body>
</html>