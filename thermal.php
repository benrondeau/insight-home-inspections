<!DOCTYPE html>
<html>
<head>
   
    <title>Thermal Imaging | Insight Inspections - Austin, TX</title>

        <!-- Header Links -->
    <?php include 'header.php'; ?> 
  
</head>
<body>
        <!-- Header Menu -->
    <?php include 'header_nav.php'; ?> 

<div class="content-area"> 
    <div class="row service-statement thermal-statement">
        <div class="pattern2"></div>
        <h1 class="whitetxt">Thermal Imaging</h1>
    </div>
    <div class="row service-info thermal-info">
        <div class="content animate-fade-in">
            <h3>Infrared cameras are the latest technology being used for fast, reliable, accurate building diagnosis.<br>Common applications include:</h3>
            <div class="col c-4">
                <div class="row">
                    <div class="col c_2">
                        <i class="icon-insulate"></i>
                    </div>
                    <div class="col c_10">
                        <h2>Insulation</h2>
                        <p>A qualified inspector can detect if there is adequate insulation in your walls and attic space by using a thermal camera. During warmer months of the year the inspector is looking for warm spots in the thermal image whereas in the winter they're looking for cool spots.</p>
                    </div>
                </div>
            </div>
            <div class="col c-4">
                <div class="row">
                    <div class="col c_2">
                        <i class="icon-droplet"></i>
                    </div>
                    <div class="col c_10">
                        <h2>Moisture</h2>
                        <p>Moisture in building materials can destroy the structural integrity and nurture mold. The first step in moisture detection is to quickly and accurately locate and remove all sources of moisture. IR cameras can often find the ultimate source with little or no physical disassembly of the premises and minimal disturbance of inhabitants.</p>
                    </div>
                </div>
            </div>             
            <div class="col c-4">
                <div class="row">
                    <div class="col c_2">
                        <i class="fa fa-bolt"></i>
                    </div>
                    <div class="col c_10">
                        <h2>Electrical</h2>
                        <p>Infrared thermogoraphy is especially helpful in locating potential problems like electrical hazards. By looking for abnormal heat emanating from an electrical system, potential problems can quickly be identified.</p>
                    </div>
                </div>
            </div>           
        </div>
    </div>
    <div class="row thermal lightbg">
        <div class="content animate-fade-in">
            <h3>Roll your mouse over the images below to see how thermal imaging works.</h3>
            <div class="c-4">
                <div class="hoverimg" title="Insulation Thermal Imaging">
                    <img class="bottom" src="images/thermal/2thm.png"><img class="top" src="images/thermal/2orig.png" height="297px">
                </div>
                <h5>Deficient insulation in ceiling.</h5>
            </div>            
            <div class="c-4">
                <div class="hoverimg" title="Water Thermal Imaging">
                    <img class="bottom" src="images/thermal/1thm.png"><img class="top" src="images/thermal/1orig.png" height="297px">                    
                </div>
                <h5>Evidence of water penetration.</h5>
            </div>
            <div class="c-4">
                <div class="hoverimg" title="Electrical Thermal Imaging">
                    <img class="bottom" src="images/thermal/3thm.png"><img class="top" src="images/thermal/3orig.png" height="297px">
                </div>
                <h5>Overheated breaker.</h5>
            </div>
        </div>
    </div>
    <div class="row ir-info whitebg">
        <div class="content animate-fade-in">
            <div class="col c-5">
                <h2>IR Fusion Technology</h2>
                <p>Insight Inspections is proud to offer Fluke's patented IR-Fusion® technology with all of our thermal inspections. IR-Fusion® technology combines both digital and infrared images all in one for better clarity. IR Fusion ® allows you to easily see, document and address problems uncovered during a thermal inspection. Fluke's unique technology provides pixel-for-pixel alignment of digital and infrared images allowing for optimal on and off-camera analysis. Many manufacturers have attempted to copy, but none have been able to match the performance and quality of Fluke's unique IR Fusion technology.</p>
            </div>
            <div class="col c-7">
                <iframe src="https://www.youtube.com/embed/wesJyzXE0WM" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="row badges">
        <div class="content">
            <div class="c-20 frame1">
                <a href="http://www.nachi.org/verify.php?nachiid=NACHI14103014">
                    <img src="http://www.nachi.org/images/logos-banners/seal-t.gif?nachiid=NACHI14103014" width="98" height="102" title="InterNachi Certified" alt="Certified by the International Association of Certified Home Inspectors" border="0"></a>
            </div>
            <div class="c-20 frame2">
                <img src="images/InfraredLogo.png" title="Infrared Certified" alt="Infrared Certified logo">
            </div>
            <div class="c-20 frame3">
                <a href="http://www.angieslist.com/AngiesList/Review/8455220" title="Angies List - Insight Inspections"><img src="http://www.angieslist.com/webbadge/PurlImage.ashx?bid=1016639e074aad9f97e4198eb3df7e8e" alt="Read Unbiased Consumer Reviews Online at AngiesList.com" width="200"></a>
            </div>
            <div class="c-20 frame4">
                <a href="http://www.yelp.com/biz/insight-inspections-austin" title="Yelp - Insight Inspections"><img alt="Insight Inspections" src="http://dyn.yelpcdn.com/extimg/en_US/rrc/kQGJof8Sj5DVWk9ZMzj5Mg.png" height="55" width="125"></a>
            </div>
            <div class="c-20 frame5">
                <a href="" title="BBB - Insight Inspections"><img alt="Insight Inspections" src="images/bbb.jpg" width="150"></a>
            </div>            
        </div>
    </div> 
    
</div><!--End Content-Area-->  
        
       <!-- Import Footer Links -->
    <?php include 'footer_menu.php'; ?>  

    <!-- Import Footer Links -->
    <?php include 'footer.php'; ?> 
       
</body>
</html>