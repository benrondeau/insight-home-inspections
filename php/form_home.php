
<?php
// Fetching Values from URL.
$name = $_POST['name'];
$email = $_POST['email'];
$message = $_POST['msg'];
$contact = $_POST['phone'];
$email = filter_var($email, FILTER_SANITIZE_EMAIL); // Sanitizing E-mail.
// After sanitization Validation is performed
if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
if (!preg_match("/^[0-9]{10}$/", $contact)) {
echo "<span>* Please Fill Valid Contact No. *</span>";
} else {
$subject = "Mail from Austinhomeinsight";
// To send HTML mail, the Content-type header must be set.
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From:' . $email. "\r\n"; // Sender's Email
$template = '<div style="padding:40px 20px;color:#111;font-size:16px;line-height:22px;"><span style="font-size:26px;">Contact Request</span><br/><br/><br/>'
. 'Name: ' . $name . '<br/>'
. 'Email: ' . $email . '<br/>'
. 'Contact No: ' . $contact . '<br/><br/>'
. 'Message:<br/>' . $message . '<br/><br/><br/>'
. 'This form was submitted from your homepage.<br/>';
$sendmessage = "<div style=\"background-color:#e4e4e4; color:#111;\">" . $template . "</div>";
// Message lines should not exceed 70 characters (PHP rule), so wrap it.
$sendmessage = wordwrap($sendmessage, 70);
// Send mail by PHP Mail Function.
mail("info@austinhomeinsight.com", $subject, $sendmessage, $headers);
echo "<script>
             alert('message sent succesfully'); 
             window.history.go(-1);
     </script>";
}
} else {
echo "<span>* invalid email *</span>";
}
?>