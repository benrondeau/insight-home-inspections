<!DOCTYPE html>
<html>
<head>
  
    <title>Contact Us | Insight Inspections - Austin, TX</title>

    <!-- Import Header Links -->
    <?php include 'header.php'; ?>
     
</head>
<body>

    <!-- Header Menu -->
    <?php include 'header_nav.php'; ?> 

<div class="content-area"> 
    <div class="row service-statement contactbg">
        <div class="pattern2"></div>
        <h1>Contact Us</h1>
    </div>
    <div class="row service-info lightbg">
        <div class="content animate-fade-in">
            <div class="col c-12 contact-form">
                <div class="col c-7 mainform2">
                    <div class="row contact-header">
                        <h2>Send us a message</h2>
                        <p>Feel free to type a message here or give us a call. We look forward to hearing from you.</p>
                    </div>
                    <form class="form2" action="php/form_contact.php" id="form" method="post" name="form">
                        <div class="row">
                            <div class="c-7 form-col"><input class="inputform2" name="name" id="name" placeholder="Name" type="text"></div>
                        </div>
                        <div class="row">       
                            <div class="c-7 form-col"><input class="inputform2" name="phone" id="phone" placeholder="Phone" type="text"></div>
                        </div>
                        <div class="row">       
                            <div class="c-7 form-col"><input class="inputform2" name="email" id="email" placeholder="Email" type="text"></div>
                        </div>                                                                 
                        <div class="row">
                            <div class="c-12 form-col">
                                <textarea class="textform2" id="msg" name="msg" placeholder="Tell Us About Your Project"></textarea>
                           </div>
                        </div>
                        <div class="row">
                           <div class="c-4 form-col">
                                <input class="inputform2 sendform2" id="send" name="submit" type="submit" value="SEND">
                           </div>
                        </div>
                    </form>  
                </div>
                <div class="col c-5 rightform">
                    <div class="row">
                        <h4>Get In Touch</h4>
                        <p>Operating Hours: Mon - Sat, 8am - 8pm</p>
                        <h5><i class="fa fa-home"></i> Location</h5>
                        <p>12407 Mopac Expwy Suite # 250-200<br>Austin TX 78758</p>
                        <h5><i class="fa fa-phone"></i> Phone</h5>
                        <p>512-638-7023</p>
                        <h5></h5>
                        <p></p>
                    </div>
                    <div class="row">
                        <div class="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3444.1014870878275!2d-97.690984!3d30.319630999999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8644cc221fbadc19%3A0x381313e45a85775a!2sInsight+Inspections!5e0!3m2!1sen!2sus!4v1431820721743" width="100%" height="320px" frameborder="0" style="border:0">
                            </iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    <div class="row badges">
        <div class="content">
            <div class="c-20 frame1">
                <a href="http://www.nachi.org/verify.php?nachiid=NACHI14103014">
                    <img src="http://www.nachi.org/images/logos-banners/seal-t.gif?nachiid=NACHI14103014" width="98" height="102" title="InterNachi Certified" alt="Certified by the International Association of Certified Home Inspectors" border="0"></a>
            </div>
            <div class="c-20 frame2">
                <img src="images/InfraredLogo.png" title="Infrared Certified" alt="Infrared Certified logo">
            </div>
            <div class="c-20 frame3">
                <a href="http://www.angieslist.com/AngiesList/Review/8455220" title="Angies List - Insight Inspections"><img src="http://www.angieslist.com/webbadge/PurlImage.ashx?bid=1016639e074aad9f97e4198eb3df7e8e" alt="Read Unbiased Consumer Reviews Online at AngiesList.com" width="200"></a>
            </div>
            <div class="c-20 frame4">
                <a href="http://www.yelp.com/biz/insight-inspections-austin" title="Yelp - Insight Inspections"><img alt="Insight Inspections" src="http://dyn.yelpcdn.com/extimg/en_US/rrc/kQGJof8Sj5DVWk9ZMzj5Mg.png" height="55" width="125"></a>
            </div>
            <div class="c-20 frame5">
                <a href="" title="BBB - Insight Inspections"><img alt="Insight Inspections" src="images/bbb.jpg" width="150"></a>
            </div>            
        </div>
    </div>   
</div><!--End Content-Area-->  
        
   <!-- Import Footer Links -->
    <?php include 'footer_menu.php'; ?>  

    <!-- Import Footer Links -->
    <?php include 'footer.php'; ?> 
         
</body>
</html>