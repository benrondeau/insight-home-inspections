<!DOCTYPE html>
<html>
<head>
 
    <title>Residential Services | Insight Inspections - Austin, TX</title>
   
      <!-- Import header Links -->
    <?php include 'header.php'; ?>  

</head>
<body>
        <!-- Header Menu -->
    <?php include 'header_nav.php'; ?> 
            
<div class="content-area"> 
    <div class="row service-statement residential">
        <div class="pattern2"></div>
        <h1 class="whitetxt animate-fade-in">Residential</h1>
    </div>
    <div class="row service-info lightbg svc">
            <div class="row">
                <div class="content animate-fade-in">
                    <div class="row">
                        <div class="c_12">
                            <h3>Buyer Inspections</h3>
                            <p>Buying a home is one of the most important purchases you will make in your lifetime, so you should be sure that the home you want to buy is in good condition. A home inspection is an evaluation of a home's condition by a state licensed and professionally trained expert. During a home inspection, our qualified inspector takes an in-depth and impartial look at the property you plan to buy. Below are just a few of the 400+ items that get evaluated on every Insight Inspection.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="c-5">
                            <ul>
                                <li>Foundations and footings</li> 
                                <li>Roof, chimney, and flashing details</li> 
                                <li>Attic and roof structure</li> 
                                <li>Gutters and downspouts</li> 
                                <li>Driveways, patios, decks and porches</li> 
                                <li>Heating and air conditioning systems</li> 
                                <li>Walls, floors, ceilings, interior windows</li> 
                                <li>Stairways</li> 
                                <li>Plumbing system including water heater</li> 
                                <li>Electrical system</li>
                                <li>Attic and insulation</li>
                                <li>Siding, trim and exterior windows</li>
                                <li>Kitchen appliances</li>
                            </ul>
                        </div>
                        <div class="c-7 homecheck">
                            <img src="images/homecheck.png" alt="Austin Home Inspection">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row border-top">
                <div class="content animate-fade-in">
                    <h3>Pre-listing Inspections</h3>
                    <p>One of the most frustrating aspects of selling a house is when a problem turns up on the physical inspection. Depending on the severity of the problem, it can lead to a new round of negotiations, delay your escrow, or even ruin your sale. A pre-listing inspection will give you a chance to see your house through a prospective buyer's eyes, giving you an opportunity to address any issues that may arise. Forewarned is forearmed, and selling your house is a stressful enough process as it is. Why not eliminate some of that stress? Other benefits include:</p>
                    <ul>
                        <li>It helps you to set the price of your house realistically, and gives you the ability to maximize the sale price.</li> 
                        <li>It permits you to make some, or all, of the needed repairs ahead of time so that defects won't become negotiating stumbling blocks later.</li> 
                        <li>It avoids delays in obtaining financing, insurance and use and occupancy permits.</li> 
                        <li>It will allow you extra time to get reasonably priced contractors or simply make the repairs yourself.</li> 
                        <li>It may encourage the buyer to waive the inspection contingency and expedite the sale process.</li> 
                        <li>It will alert you of items of immediate personal concern, such as asbestos, radon gas or active termite infestation.</li> 
                        <li>It will relieve the prospective buyer's concerns and suspicions.</li> 
                        <li>It reduces your liability by adding professional supporting documentation to your disclosure statement.</li> 
                        <li>It alerts you to immediate safety issues before agents and visitors tour your home.</li> 
                    </ul>  
                </div>
            </div>
            <div class="row border-top">    
                <div class="content animate-fade-in">
                    <h3>Foreclosed Property Inspections</h3>
                    <p>In the last couple of years we are currently seeing an unprecedented wave of foreclosures. Though this means hard times for the homeowners and lenders, it could mean a windfall for home buyers or investors.  Buying a foreclosed home or commercial property, however, can come with its own special set of problems. It is extremely important when considering purchasing a foreclosed property that a full home inspection is carried out to the highest degree. The foreclosed property will be offered for sale in an “As Is” condition, this means what you see is what you get including all the hidden defects and damage. If you purchase a foreclosed property without a home inspection you will assume all the risk and liability when closing the deal.</p>
                </div>
            </div>

    </div>
    <div class="row badges">
        <div class="content">
            <div class="c-20 frame1">
                <a href="http://www.nachi.org/verify.php?nachiid=NACHI14103014">
                    <img src="http://www.nachi.org/images/logos-banners/seal-t.gif?nachiid=NACHI14103014" width="98" height="102" title="InterNachi Certified" alt="Certified by the International Association of Certified Home Inspectors" border="0"></a>
            </div>
            <div class="c-20 frame2">
                <img src="images/InfraredLogo.png" title="Infrared Certified" alt="Infrared Certified logo">
            </div>
            <div class="c-20 frame3">
                <a href="http://www.angieslist.com/AngiesList/Review/8455220" title="Angies List - Insight Inspections"><img src="http://www.angieslist.com/webbadge/PurlImage.ashx?bid=1016639e074aad9f97e4198eb3df7e8e" alt="Read Unbiased Consumer Reviews Online at AngiesList.com" width="200"></a>
            </div>
            <div class="c-20 frame4">
                <a href="http://www.yelp.com/biz/insight-inspections-austin" title="Yelp - Insight Inspections"><img alt="Insight Inspections" src="http://dyn.yelpcdn.com/extimg/en_US/rrc/kQGJof8Sj5DVWk9ZMzj5Mg.png" height="55" width="125"></a>
            </div>
            <div class="c-20 frame5">
                <a href="" title="BBB - Insight Inspections"><img alt="Insight Inspections" src="images/bbb.jpg" width="150"></a>
            </div>            
        </div>
    </div>     

    
</div><!--End Content-Area--> 

    <!-- Import Footer -->
    <?php include 'footer_menu.php'; ?> 
        
    <!-- Import Footer -->
    <?php include 'footer.php'; ?>
       
</body>
</html>