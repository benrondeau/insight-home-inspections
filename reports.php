<!DOCTYPE html>
<html>
<head>
   
    <title>Sample Reports | Insight Inspections - Austin, TX</title> 

    <!-- Import Header Links -->
    <?php include 'header.php'; ?>   
 
</head>
<body>
    <!--EMAIL GATE DIV OVERLAY-->
    <div id="email-gate">
        <div id="light" class="mainform3 animate-fade-in">
            <div class="contact-head">        
                <h3>Looking for Sample Reports?</h3><h4>Enter a valid email address for access.</h4>
            </div>
            <form class="form3" action="php/email_gate.php" id="form" method="post" name="form">
                <div class="row">       
                    <div class="c-12 form-col"><input class="inputform3" name="email" id="email" placeholder="Email" type="text"></div>
                </div>                
                <div class="row">
                   <div class="c-12 form-col">
                        <input class="inputform3 sendform3" id="send" name="submit" type="submit" value="SEND">
                   </div>
                </div>
            </form>
         </div>                    
    </div>     
    
    <!-- Import Footer Links -->
    <?php include 'footer.php'; ?>
    
</body>
</html>