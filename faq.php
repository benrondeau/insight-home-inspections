<!DOCTYPE html>
<html>
<head>   
 
    <title>Frequently Asked Questions | Insight Inspections - Austin, TX</title>

    <!-- Import Header Links -->
    <?php include 'header.php'; ?>
 
</head>
<body>
    
    <!-- Header Menu -->
    <?php include 'header_nav.php'; ?> 

<div class="content-area"> 
    <div class="row service-statement faqbg">
        <div class="pattern2"></div>
        <h1 class="hidden visible-xs">FAQ:</h1>
        <h1 class="hidden-xs">Frequently Asked Questions:</h1>
    </div>
    <div class="row service-info lightbg faq">
        <div class="content animate-fade-in">
            <div class="col c-12">
                <h3>What is a home inspection?</h3>
                <p>A home inspection is an objective visual examination of the physical structure and systems of a home, from the roof to the foundation. The standard home inspector's report will include an evaluation of the condition of the home's heating system, central air conditioning system (temperature permitting), interior plumbing and electrical systems; the roof, attic, and visible insulation; walls, ceilings, floors, windows and doors; the foundation, basement, and visible structure.</p>
                <h3>Why do I need an inspection?</h3>
                <p>The purchase of a home is probably the largest single investment you will ever make. You should learn as much as you can about the condition of the property and the need for any major repairs before you buy, so that you can minimize unpleasant surprises and difficulties afterwards.Of course, an inspection will also point out positive aspects of a home, as well as some maintenance that will be necessary to keep it in good shape. After the inspection, you will have a much clearer understanding of the property you are about to purchase, and will be able to make a confident buying decision. If you have owned your home for a long time, an inspection can identify problems in the making and recommend preventive measures which might avoid costly future repairs. In addition, home sellers may opt for having an inspection prior to placing the home on the market to gain a better understanding of conditions which the buyer's inspector may point out. This provides an opportunity to make repairs that will put the house in better selling condition.</p>
                <h3>What will an inspection cost?</h3>
                <p>The cost of an inspection varies based upon a number of factors, including size, age, special services requested, etc. However, do not let cost be a factor in deciding whether or not to have a inspection, or in the selection of your home inspector. The knowledge gained from an inspection is well worth the cost, and the lowest-priced inspector is not necessarily a bargain. Rather, you should consider the inspection as an investment that will pay for itself many times over. You do not want the life-changing decision of buying a home to be something to regret.</p>
                <h3>Can't I do it myself?</h3>
                <p>Even the most experienced home owner lacks the knowledge, objectivity and expertise of a professional home inspector who has inspected thousands of homes in his or her career. An inspector is familiar with all the elements of home construction, their proper installation, and maintenance. He or she understands how the home's systems and components are intended to function together, as well as how and why they fail. Above all, most buyers find it very difficult to remain completely objective and unemotional about the house they really want, and this may affect their judgment. For the most accurate picture, it is best to obtain an impartial third-party opinion by an expert in the field of inspection.</p>
                <h3>Can a house fail inspection?</h3>
                <p>No. A professional inspection is an examination of the current condition of your prospective home. It is not an appraisal, which determines market value, or a municipal inspection, which verifies local code compliance. A home inspector, therefore, will not pass or fail a house, but rather describe its physical condition.</p>
                <h3>When do I call in the home inspector?</h3>
                <p>A home inspector is typically called right after the contract or purchase agreement has been signed, and is often available within a few days. However, before you sign, be sure that there is an inspection clause in the contract, making your purchase obligation contingent upon the findings of a professional inspection. This clause should specify the terms to which both the buyer and seller are obligated.</p>
                <h3>Do I have to be there?</h3>
                <p>It's not necessary for you to be present for the inspection, but it is recommended you be there for the inspection wrap-up. By observing and asking questions at the wrap-up, you will learn a great deal about the condition of the home, how its systems work, and how to maintain it. You will also find the written report easier to understand if you've seen the property first-hand through the inspector's eyes.</p>
                <h3>What if the report reveals problems?</h3>
                <p>No house is perfect. If the inspector finds problems, it doesn't necessarily mean you shouldn't buy the house, only that you will know in advance what to expect. A seller may be flexible with the purchase price or contract terms if major problems are found. If your budget is very tight, or if you don't wish to become involved in future repair work, this information will be extremely important to you.</p>
                <h3>Can a Seller have their house inspected prior to listing the house for sale?</h3>
                <p>Absolutely! A pre-listing inspection can identify concerns with the house and give the seller the opportunity to correct the problems prior to the sale.  In this way, there will be no ‘suprises’ that come up during the buyer’s inspection.  Concerns identified by the buyer’s inspection are often blown out of proportion due to the additional stress and time constraints involved with the real estate transaction.  The pre-listing inspection allows the seller to address concerns in a rational and effective manner outside the real estate transaction environment thus ensuring the successful sale of your home.</p> 
                <h3>If the house proves to be in good condition, did I really need an inspection?</h3>
                <p>Definitely. Now you can complete your home purchase with peace of mind about the condition of the property and all its equipment and systems. You will also have learned a few things about your new home from the inspector's report, and will want to keep that information for future reference. Above all, you can feel assured that you are making a well-informed purchase decision, and that you will be able to enjoy your new home the way you want to.</p>
                <h3>Do I need an inspection on a brand new house?</h3>
                <p>Although builders do offer a warranty on their home, it is up to the buyer to identify problems to be corrected by the builder.  Generally, builders allow the buyer to complete a walk through inspection to identify cosmetic concerns that are readily visible to the new homeowner.  A professional inspection identifies functional problems related to the major systems that would not be identified during the walkthrough inspection.  For example, is there sufficient insulation in the attic, are the electrical plugs wired properly and GFCI protected where required, roof issues, structural problems, etc.  The new home inspection allows you to correct problems under the builder warranty, save you costly repairs and to provide you with peace of mind after you move in and when you eventually sell the house.</p>
            </div>
        </div>
    </div>
    <div class="row badges">
        <div class="content">
            <div class="c-20 frame1">
                <a href="http://www.nachi.org/verify.php?nachiid=NACHI14103014">
                    <img src="http://www.nachi.org/images/logos-banners/seal-t.gif?nachiid=NACHI14103014" width="98" height="102" title="InterNachi Certified" alt="Certified by the International Association of Certified Home Inspectors" border="0"></a>
            </div>
            <div class="c-20 frame2">
                <img src="images/InfraredLogo.png" title="Infrared Certified" alt="Infrared Certified logo">
            </div>
            <div class="c-20 frame3">
                <a href="http://www.angieslist.com/AngiesList/Review/8455220" title="Angies List - Insight Inspections"><img src="http://www.angieslist.com/webbadge/PurlImage.ashx?bid=1016639e074aad9f97e4198eb3df7e8e" alt="Read Unbiased Consumer Reviews Online at AngiesList.com" width="200"></a>
            </div>
            <div class="c-20 frame4">
                <a href="http://www.yelp.com/biz/insight-inspections-austin" title="Yelp - Insight Inspections"><img alt="Insight Inspections" src="http://dyn.yelpcdn.com/extimg/en_US/rrc/kQGJof8Sj5DVWk9ZMzj5Mg.png" height="55" width="125"></a>
            </div>
            <div class="c-20 frame5">
                <a href="" title="BBB - Insight Inspections"><img alt="Insight Inspections" src="images/bbb.jpg" width="150"></a>
            </div>            
        </div>
    </div>   
</div><!--End Content-Area-->  
        
   <!-- Import Footer Links -->
    <?php include 'footer_menu.php'; ?>  

    <!-- Import Footer Links -->
    <?php include 'footer.php'; ?> 
         
</body>
</html>