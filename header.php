<!-- Meta Information -->
<meta charset='UTF-8'>
<meta name="description" content="Austin home inspection">
<meta name="keywords" content="Real Estate Inspection, New Home Inspection, 1 (One) Year Warranty Inspection, Phase Inspection, Thermal Imaging, Thermal Imaging Home Inspection, Thermal Imaging Austin, Free Thermal Imaging, Property Inspection, Insight Austin, Best Home Inspection, Discount Home Inspection, Affordable Home Inspection, Home Inspection Yelp, 5 star Inspection, Commercial Property Inspection, Warranty Inspection Austin, BBB Home Inspection, Home Advisor Home Inspection Austin, Austin home inspection">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="HandheldFriendly" content="true">  
<meta name="format-detection" content="telephone=no">    
<link rel="canonical" href="http://austinhomeinsight.com/" />    
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Insight Inspections - Home Inspectors - Austin, TX" />
<meta property="og:description" content="Insight Inspections is a full service inspection group with a focus on educating home buyers through clear communication and detailed assessments." />
<meta property="og:url" content="http://austinhomeinsight.com/" />
<meta property="og:site_name" content="Insight Inspections" />
<meta property="article:publisher" content="https://www.facebook.com/insightaustin" />
<meta property="og:image" content="http://austinhomeinsight.com/images/favicon.png" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="Insight Inspections is a full service inspection group with a focus on educating home buyers through clear communication and detailed assessments."/>
<meta name="twitter:title" content="Insight Inspections - Home Inspectors - Austin, TX"/>
<meta name="twitter:site" content="@insight_inspect"/>
<meta name="twitter:domain" content="Insight Inspections"/>
<meta name="twitter:image:src" content="http://austinhomeinsight.com/images/favicon.png"/>
<meta name="twitter:creator" content="@insight_inspect"/> 

<!-- Favicon -->
<link rel="icon" type="image/png" href="images/favicon.png">  

<!-- CSS -->
<link href="push-framework/push-grid.css" rel="stylesheet">
<link href="push-framework/push-animate.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/slider.css" rel="stylesheet">    
<link rel="stylesheet" href="css/font-awesome.css">

<!-- Fonts -->
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

<!-- Google Analytics Script -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-57116219-1', 'auto');
ga('send', 'pageview');
</script> 