    <div class="container">
        <div class="row nav-upper">
            <div class="content">
                <ul class="animate-fade-in">
                    <li><a class="phone" href="tel:+15126387023">512-638-7023</a></li>
                    <li><a href="tel:+15126387023"><i class="fa fa-phone"></i></a></li>
                    <li><a href="mailto:info@austinhomeinsight.com"><i class="fa fa-envelope"></i></a></li>
                </ul>
            </div>
        </div>
    <div class="nav-main whitebg">
        <div class="content animatedParent">
            <a class="nav-logo animate-fade-in" href="index.php"><img src="images/logo.png" alt="Austin Home Insight logo"></a>
            <div class="hidden-lg">
                <ul class="animate-fade-in">
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="residential.php">SERVICES</a>
                        <ul class="animated fadeInDown">
                            <li><a href="residential.php">RESIDENTIAL</a></li>
                            <li><a href="thermal.php">THERMAL IMAGING</a></li>
                            <li><a href="commercial.php">COMMERCIAL</a></li>
                        </ul>
                    </li>
                    <li><a href="about.php">ABOUT</a></li>                    
                    <li><a href="reports.php">SAMPLE REPORTS</a></li>
                    <li><a href="faq.php" title="Frequently Asked Questions">FAQ</a></li>
                    <li><a href="contact.php">CONTACT</a></li>
                </ul>
            </div>
            <div class="mobile"><div class="toggle"><i class="fa fa-bars"></i></div></div> 
        </div><!--End Content-->
    </div><!--End Nav Main-->
    <div class="nav-placeholder nodisplay"></div> 