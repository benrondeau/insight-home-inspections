<!DOCTYPE html>
<html>
<head>
   
    <title>Commercial Services | Insight Inspections - Austin, TX</title>

    <!-- Import Header Links -->
    <?php include 'header.php'; ?>
    
</head>
<body>
    
    <!-- Header Menu -->
    <?php include 'header_nav.php'; ?> 
       

<div class="content-area"> 
    <div class="row service-statement commercial">
        <div class="pattern2"></div>
        <h1 class="whitetxt animate-fade-in">Commercial</h1>
    </div>
    <div class="row service-info whitebg svc">
        <div class="row">
            <div class="content animate-fade-in">
                <div class="row">
                    <div class="c_12">
                        <h3>Services:</h3>
                        <p>Insight Inspections can provide you with an educated evaluation of a commercial building and any accompanying components. Our staff of highly trained consultants can evaluate the property and advise you of the overall condition and make recommendations regarding any maintenance, or planned upgrades. The commercial inspection involves a comprehensive evaluation of all major components of the building including the roof, electrical system, structure, interior and exterior elements, building envelope, HVAC systems, plumbing and more. We are certain that we will exceed your expectations with our level of detail and thoroughness. Insight Inspections offers commercial building inspections of:</p>
                    </div>
                </div>
                <div class="row">
                    <div class="c-5">
                        <ul>
                            <li>Strip centers</li> 
                            <li>Motels</li> 
                            <li>Office buildings</li> 
                            <li>Warehouse facilities</li> 
                            <li>Manufacturing facilities</li> 
                            <li>Multi-family buildings</li> 
                            <li>Condominium buildings</li> 
                            <li>Apartment complexes</li> 
                            <li>Office condominiums</li> 
                            <li>Restaurants</li>
                        </ul>
                    </div>
                    <div class="c-7 helmetreport">
                        <img src="images/helmetreport.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row badges">
        <div class="content">
            <div class="c-20 frame1">
                <a href="http://www.nachi.org/verify.php?nachiid=NACHI14103014">
                    <img src="http://www.nachi.org/images/logos-banners/seal-t.gif?nachiid=NACHI14103014" width="98" height="102" title="InterNachi Certified" alt="Certified by the International Association of Certified Home Inspectors" border="0"></a>
            </div>
            <div class="c-20 frame2">
                <img src="images/InfraredLogo.png" title="Infrared Certified" alt="Infrared Certified logo">
            </div>
            <div class="c-20 frame3">
                <a href="http://www.angieslist.com/AngiesList/Review/8455220" title="Angies List - Insight Inspections"><img src="http://www.angieslist.com/webbadge/PurlImage.ashx?bid=1016639e074aad9f97e4198eb3df7e8e" alt="Read Unbiased Consumer Reviews Online at AngiesList.com" width="200"></a>
            </div>
            <div class="c-20 frame4">
                <a href="http://www.yelp.com/biz/insight-inspections-austin" title="Yelp - Insight Inspections"><img alt="Insight Inspections" src="http://dyn.yelpcdn.com/extimg/en_US/rrc/kQGJof8Sj5DVWk9ZMzj5Mg.png" height="55" width="125"></a>
            </div>
            <div class="c-20 frame5">
                <a href="" title="BBB - Insight Inspections"><img alt="Insight Inspections" src="images/bbb.jpg" width="150"></a>
            </div>            
        </div>
    </div>    
    
</div><!--End Content-Area-->  
        
   <!-- Import Footer Links -->
    <?php include 'footer_menu.php'; ?>  

    <!-- Import Footer Links -->
    <?php include 'footer.php'; ?> 
        
</body>
</html>