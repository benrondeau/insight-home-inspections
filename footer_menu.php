<div class="row footer-upper">
        <div class="content animate-fade-in">
            <div class="col c-3">
                <h4>SITE MAP</h4>
                <ul>
                    <li><a href="index.php"><i class="fa fa-angle-right"></i> HOME</a></li>
                    <li><a href="residential.php"><i class="fa fa-angle-right"></i> RESIDENTIAL</a></li>
                    <li><a href="thermal.php"><i class="fa fa-angle-right"></i> THERMAL IMAGING</a></li>
                    <li><a href="commercial.php"><i class="fa fa-angle-right"></i> COMMERCIAL</a></li>
                    <li><a href="reports.php"><i class="fa fa-angle-right"></i> SAMPLE REPORTS</a></li>
                    <li><a href="faq.php"><i class="fa fa-angle-right"></i> FAQ</a></li>
                    <li><a href="about.php"><i class="fa fa-angle-right"></i> ABOUT</a></li>
                    <li><a href="contact.php"><i class="fa fa-angle-right"></i> CONTACT</a></li>
                </ul>
            </div>
            <div class="col c-3">
                <h4>GET IN TOUCH</h4>
                <h5><i class="fa fa-home"></i> Location</h5>
                <p>12407 Mopac Expwy Suite # 250-200<br>Austin TX 78758</p>
                <h5><i class="fa fa-phone"></i> Phone</h5>
                <p><a href="tel:+15126387023">512-638-7023</a></p>
                <h5>TREC# 21271</h5>                
            </div>
            <div class="col c-3">
                <h4>ABOUT US</h4>
                <p>Insight Inspections is owned, based, and operated out of Austin Texas. Our appointment times are flexible and include both evening and weekend hours. We serve the entire Austin metro area as well as the surrounding regions. Scheduling is quick, easy and for your convenience, appointments are available 7 days a week. To schedule an appointment <a href="contact.php">click here</a></p>
            </div>         
            <div class="col c-3 social-media">
                <h4>SOCIAL MEDIA</h4>
                <ul>
                    <li><a href="https://www.facebook.com/insightaustin"><i class="fa fa-facebook-official"></i></a></li>
                    <li><a href="https://twitter.com/insight_inspect"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.linkedin.com/pub/scott-swayze/21/15a/897"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>       
        </div>      
    </div> 
    <div class="row footer-lower">
        <div class="content">
            <h6 class="left">Copyright <i class="fa fa-copyright"></i> 2014-2015 Insight Inspections, All Rights Reserved.  Design by <a href="http://www.pushgfx.com">Push Graphics</a></h6>
        </div>
    </div>        
    <!--End FOOTER-->   

    </div><!--End Container--> 
    <!--MOBILE MENU-->
    <div class="mobile-menu">
        <div class="row logo"><a href="#"><img src="images/logo.png"/></a></div>
        <div class="menu"><div class="menu-main-container">
            <ul id="menu-main-1" class="nav">
                <li><a href="index.php">HOME</a></li>
                <li><a href="residential.php">RESIDENTIAL</a></li>
                <li><a href="thermal.php">THERMAL IMAGING</a></li>
                <li><a href="commercial.php">COMMERCIAL</a></li> 
                <li><a href="about.php">ABOUT</a></li>                
                <li><a href="reports.php">SAMPLE REPORTS</a></li> 
                <li><a href="faq.php">FAQ</a></li>  
                <li><a href="contact.php">CONTACT</a></li>             
            </ul></div></div>
        <div class="row social">
            <ul>
                <li><a href="tel:+15126387023"><i class="fa fa-phone"></i></a></li>
                <li><a href="https://www.facebook.com/insightaustin"><i class="fa fa-facebook-official"></i></a></li>
                <li><a href="https://twitter.com/insight_inspect"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.linkedin.com/pub/scott-swayze/21/15a/897"><i class="fa fa-linkedin"></i></a></li>
            </ul>
        </div>
        <div class="row return">
            <a href="#"><div class="toggle"></div></a>
        </div>
    </div> 