<!DOCTYPE html>
<html>
<head>

    <!-- Page Title -->
    <title>Insight Inspections - Austin, TX</title>

    <!-- Import Header Links -->
    <?php include 'header.php'; ?>

</head>
<body>

    <!-- Header Menu -->
    <?php include 'header_nav.php'; ?>    

    <div class="content-area"> 

    <div class="row slider-wrap">
     <!-- Jssor Slider Begin -->
        <!-- To move inline styles to css file/block, please specify a class name for each element. --> 
        <div id="slider1_container" class="slider1_container hidden-xs">

            <!-- Slides Container -->
            <div u="slides" class="slides">
                <div>
                    <img u="image" src="images/atxskyline.jpg" />
                </div>
                <div>
                    <img u="image" src="images/homelarge.jpg" />
                </div>            
                <div>
                    <img u="image" src="images/austinskyline.jpg" />
                </div>            
            </div>
    <div class="pattern1"></div>           
        </div>
        <!-- Jssor Slider End -->
        <div class="row"><!--XL VERSION-->
            <div class="content animate-fade-in">
                <div class="col c-8 lg statement">
                    <h1><span>Experience You Can Trust</span></h1>
                    <h4>The decision to purchase a home is one of the largest and most exciting financial decisions of your life. Finding a home inspector that you trust is an essential part of that process. We inspect every home with the utmost care, and take time to explain our findings clearly while answering all of your questions and concerns. Your home inspector will provide you with a comprehensive report, complete with high-quality photos as well as detailed descriptions for every major component of your home or business. A state of the art thermal imaging scan is included on EVERY Insight inspection and with our 100% satisfaction guarantee, you can be sure an Insight inspection will give you all the information needed to make an educated real estate decision.</h4>
                    <div class="thermal-badge"><a href="thermal.php" title="Thermal Imaging"><img src="images/thermalbadge.png" alt="Thermal Imaging"></a></div>
                </div>
                <div id="light" class="c-4 lg mainform animate-fade-in">
                    <div class="contact-head1">
                        <h2><a href="tel:+15126387023">512-638-7023</a></h2>
                    </div>
                    <div class="contact-head2">
                        <h3>Have a question?<br>Need a <span>FREE QUOTE</span>?</h3><h4>An Insight professional is never<br>more than a call or click away!</h4>
                    </div>
                    <form class="form1" action="php/form_home.php" id="form" method="post" name="form">
                        <div class="row">
                            <div class="c-12 form-col"><input class="inputform1" name="name" id="name" placeholder="Name" type="text"></div>
                        </div>
                        <div class="row">       
                            <div class="c-12 form-col"><input class="inputform1" name="phone" id="phone" placeholder="Phone" type="text"></div>
                        </div>
                        <div class="row">       
                            <div class="c-12 form-col"><input class="inputform1" name="email" id="email" placeholder="Email" type="text"></div>
                        </div>                
                        <div class="row">
                            <div class="c-12 form-col">
                                <textarea class="textform1" id="msg" name="msg" placeholder="Message"></textarea>
                           </div>
                        </div>
                        <div class="row">
                           <div class="c-12 form-col">
                                <input class="inputform1 sendform1" id="send" name="submit" type="submit" value="SEND">
                           </div>
                        </div>
                    </form>
                 </div>        
            </div>
        </div>    
    </div>  
    <div class="row services medgradbg">       
        <div class="content animate-fade-in">
            <h1>Services</h1>
            <div class="c-4 services-pane">
                <a class="sp-helper" href="residential.php" title="Residential"></a>      
                <h2>RESIDENTIAL</h2>
                <img src="images/paperhouse.jpg">      
                <div class="services-pane-bar"><span>LEARN MORE</span><i class="fa fa-long-arrow-right"></i></div>
            </div>    
            <div class="c-4 services-pane">  
                <a class="sp-helper" href="thermal.php" title="Thermal Imaging"></a>      
                <h2>THERMAL IMAGING</h2>
                <img src="images/thermal_gun.jpg">      
                <div class="services-pane-bar"><span>LEARN MORE</span><i class="fa fa-long-arrow-right"></i></div>
            </div>                               
            <div class="c-4 services-pane"> 
                <a class="sp-helper" href="commercial.php" title="Commercial"></a>      
                <h2>COMMERCIAL</h2>
                <img src="images/contractors.jpg">      
                <div class="services-pane-bar"><span>LEARN MORE</span><i class="fa fa-long-arrow-right"></i></div>
            </div>                                  
        </div>
    </div>
    <div class="row badges">
        <div class="content">
            <div class="c-20 frame1">
                <a href="http://www.nachi.org/verify.php?nachiid=NACHI14103014">
                    <img src="http://www.nachi.org/images/logos-banners/seal-t.gif?nachiid=NACHI14103014" width="98" height="102" title="InterNachi Certified" alt="Certified by the International Association of Certified Home Inspectors" border="0"></a>
            </div>
            <div class="c-20 frame2">
                <img src="images/InfraredLogo.png" title="Infrared Certified" alt="Infrared Certified logo">
            </div>
            <div class="c-20 frame3">
                <a href="http://www.angieslist.com/AngiesList/Review/8455220" title="Angies List - Insight Inspections"><img src="http://www.angieslist.com/webbadge/PurlImage.ashx?bid=1016639e074aad9f97e4198eb3df7e8e" alt="Read Unbiased Consumer Reviews Online at AngiesList.com" width="200"></a>
            </div>
            <div class="c-20 frame4">
                <a href="http://www.yelp.com/biz/insight-inspections-austin" title="Yelp - Insight Inspections"><img alt="Insight Inspections" src="http://dyn.yelpcdn.com/extimg/en_US/rrc/kQGJof8Sj5DVWk9ZMzj5Mg.png" height="55" width="125"></a>
            </div>
            <div class="c-20 frame5">
                <a href="" title="BBB - Insight Inspections"><img alt="Insight Inspections" src="images/bbb.jpg" width="150"></a>
            </div>            
        </div>
    </div>     

    </div><!--End Content-Area-->  

   <!-- Import Footer Links -->
    <?php include 'footer_menu.php'; ?>  

    <!-- Import Footer Links -->
    <?php include 'footer.php'; ?> 
    
   
</body>
</html>