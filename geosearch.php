<?php
	$post_site = "http://!ahi2015:2015ahi!@geosearchseo.co/GeoSearch/getcontentsPHP3.php?siteid=180";
	$framework=true;//TODO: determine if the site is running our framework, automatically.
	foreach($_GET as $k => $val)
		$_GET[$k] = urlencode($val);
	if(isset($_GET['word']) && strlen($_GET['word']) > 0){
		//Load the final template
		$runtype = '3';
		$post_site .= '&cat='.$_GET['cat'].'&word='.urlencode($_GET['word']);
	}elseif(isset($_GET['cat']) && strlen($_GET['cat']) > 0 && isset($_GET['pagenumber']) && strlen($_GET['pagenumber']) > 0){
		//Load the words template
		$runtype = '2';
		$post_site .= '&cat='.$_GET['cat'].'&pagenumber='.$_GET['pagenumber'];
	}else{
		//defualt - get the categories
		$runtype = '1';
		$post_site .= '&cat='.$_GET['cat'].'&pagenumber='.$_GET['pagenumber'];
	}
	$ch = curl_init();
	//print $post_site.'&runtype='.$runtype;
	curl_setopt($ch, CURLOPT_URL, $post_site.'&runtype='.$runtype);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$response = curl_exec($ch);
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	if($httpCode == 404) {
		//Ensure a 404 is returned, no matter what.
		header("HTTP/1.0 404 Not Found");
		//TODO: perhaps domin8 can locate, and save the site's friendly 404 page, and return it.
		if($framework){
			//Let the framework display a freindly 404 page.
			$_GET['route'] = 'error/not_found';
			include('index.php');
		}else{
			if(!empty($response))
				echo $response;
			else
				print "I'm sorry, you have requested a page that has either moved, or no longer exists.";
		}
	}else{
		print $response;
	}
	curl_close($ch);
	
	if(!function_exists('print_array')){
		function print_array($array){
			print "<pre>";
			print_r($array);
			print "</pre>";
		}
	}
	//main_template can be built dynamically if we want
	//It should setup the look and feel of the site, using the $contents array
	//include('main_template.tpl');
?>