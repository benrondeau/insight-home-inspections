<!DOCTYPE html>
<html>
<head>
   
    <title>Sample Reports | Insight Inspections - Austin, TX</title>

    <!-- Import Header Links -->
    <?php include 'header.php'; ?> 
 
</head>
<body>
        <!-- Header Menu -->
    <?php include 'header_nav.php'; ?> 
            
        <div class="content-area"> 
            <div class="row service-statement reportsbg">
                <div class="pattern2"></div>
                <h1>Sample Reports</h1>
            </div>
            <div class="row service-info lesslightbg faq">
                <div id="pdf"><iframe src="pdf/samplereport.pdf">Click here to download the PDF</iframe></div>
            </div>
    <div class="row badges">
        <div class="content">
            <div class="c-20 frame1">
                <a href="http://www.nachi.org/verify.php?nachiid=NACHI14103014">
                    <img src="http://www.nachi.org/images/logos-banners/seal-t.gif?nachiid=NACHI14103014" width="98" height="102" title="InterNachi Certified" alt="Certified by the International Association of Certified Home Inspectors" border="0"></a>
            </div>
            <div class="c-20 frame2">
                <img src="images/InfraredLogo.png" title="Infrared Certified" alt="Infrared Certified logo">
            </div>
            <div class="c-20 frame3">
                <a href="http://www.angieslist.com/AngiesList/Review/8455220" title="Angies List - Insight Inspections"><img src="http://www.angieslist.com/webbadge/PurlImage.ashx?bid=1016639e074aad9f97e4198eb3df7e8e" alt="Read Unbiased Consumer Reviews Online at AngiesList.com" width="200"></a>
            </div>
            <div class="c-20 frame4">
                <a href="http://www.yelp.com/biz/insight-inspections-austin" title="Yelp - Insight Inspections"><img alt="Insight Inspections" src="http://dyn.yelpcdn.com/extimg/en_US/rrc/kQGJof8Sj5DVWk9ZMzj5Mg.png" height="55" width="125"></a>
            </div>
            <div class="c-20 frame5">
                <a href="" title="BBB - Insight Inspections"><img alt="Insight Inspections" src="images/bbb.jpg" width="150"></a>
            </div>            
        </div>
    </div> 
</div><!--End Content-Area-->  

   <!-- Import Footer Links -->
    <?php include 'footer_menu.php'; ?>  

    <!-- Import Footer Links -->
    <?php include 'footer.php'; ?>  
         
</body>
</html>